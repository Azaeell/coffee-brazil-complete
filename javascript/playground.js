const openBUTTON = document.querySelector('.open');
const closeBUTTON = document.querySelector('.close');
const modal = document.querySelector('.layout');
const body = document.querySelector('body');

const handleToggleModal = () => {
    modal.show();
};

const close = () => {
    modal.close();
};

openBUTTON.addEventListener('click', handleToggleModal);
closeBUTTON.addEventListener('click', close);

console.log